// A multi-level game where the player kills monsters, a dragon boss, and becomes a hero
//Bailey Benjamin

import java.util.Scanner;
import java.util.Random;

public class Homework3V1
   {
      // all the values. ALL of them.
      public enum Status {CONTINUE, LOSE, WIN, QUIT}
      public static Scanner input = new Scanner(System.in);
      public static Status gameStatus;
      //public static int userSelect;
      public static int choice;
      public static int points;
      public static int number;
      
      public static boolean isFighting = true;
      
      
      // FINAL values
      public static final int ADVENTURE = 1;
      public static final int DRAGON = 2;
      public static final int QUIT = 3;
      
      public static final int BUY_HP = 1;
      public static final int BUY_AP = 2;
      public static final int BUY_MP = 3;
      
      
      public static final int WOLF = 0;
      public static final int CULTIST = 1;
      public static final int FARMER = 2;
      
      public static final int MELEE = 1;
      public static final int MAGIC = 2;
      public static final int CHARGE = 3;
      public static final int RUN = 4;
      
   //hero data variables when I turned these all to 100, the dragon still defeated me.
      public static int heroLevel = 1;
      public static int heroHealth = 0;
      public static int heroAttack = 0;
      public static int heroMagic = 0;
   
   //monster variables
      public static String monsterName;
      public static int monsterHealth = 0;
      public static int monsterAttack = 0;
      public static int monsterExperience = 0;
      
      public static Random Rand = new Random();
   
   
      //My main method! so cute!!! It runs all of my methods.
      public static void main(String[] main)
      
      
      {
         int userSelect;
         
         System.out.println("Hello, Goat. Your Flock is being harassed by Monsters! You must act quickly!");
         gameStatus = Status.CONTINUE;
      
          createCharacter();
         
         while (gameStatus == Status.CONTINUE)
         {
         printMainPrompt();
         userSelect = input.nextInt();
         executeAdventureChoice(userSelect);
         }
      
      // print end message?
      }
   
      public static void createCharacter () {
   //create a counter‐controlled repetition, starts at 20 stat points and stops at 0
         for(points = 20; points >= 1; points--){
         printCreationPrompt(points);
        int number = input.nextInt();
        executeStatChoice(number);

      }
   }
   
      public static void printCreationPrompt (int elpointo) {
         System.out.println();
               
//Print the current values for hero’s health, attack and magic power
         System.out.println("1. +10 Health");
         System.out.println("2. +1 Attack");
         System.out.println("3. +3 Magic");
//Printthe number of stat points that player has left (based on parameter)
      System.out.println("You have " + elpointo + " points to spend.");
      System.out.println("Goat: Health: " + heroHealth + " Attack: " + heroAttack + " Magic: " + heroMagic);
//end printCreationPrompt
      }
      
      public static int executeStatChoice (int number) {
        int pointsRefund = 0;
         System.out.println();

//if choice is BUY_HP
//add 10 to hero’s health
      if (number == BUY_HP)
      {
      heroHealth += 10;
      }
//else if choice is BUY_AP
//add 1 to hero’s attack power
      else if (number == BUY_AP)
      {
         heroAttack += 1;
      }
//else if choice is BUY_MP
//add 3 to hero’s magicpower
      else if (number == BUY_MP)
      {
         heroMagic += 3;
      }
//else
//Print “not a valid option”
      else
      {
         System.out.println("ONE through THREE, Dingus.");
            pointsRefund += 1;
            points += pointsRefund;
      }
//increment local variable for points refunded
//refund the points
//end executeStatChoice method
           return pointsRefund;
      }
   
      public static void printMainPrompt () {
      
      System.out.println("It's not safe for a goat like you! What will you do?");
      System.out.println("1.) FIND ADVENTURE");
      System.out.println("2.) CONFRONT THE DRAGON");
      System.out.println("3.) QUIT LIKE A LOSER");
      }
   
      public static void executeAdventureChoice (int choice) {
         
      //if user input is equal to named constantADVENTURE
         if (choice == ADVENTURE)
         {
            //createarandommonster(invokemethod:generateRandomMonster
            generateRandomMonster();
            //runthecombatloop(invokemethod:runCombatLoop)
            runCombatLoop();
         }


//else if user input is equal to named constant DRAGON
         else if (choice == DRAGON)
         {
         //create a dragon (invokemethod:createDragon)
         createDragon();

         //run the combat loop (invoke method: runCombatLoop)
         runCombatLoop();
         }
//else if user input is equal to named constant QUIT
         else if (choice == QUIT)
         {
         gameStatus = Status.QUIT;
         }
//else
         else 
         {
            System.out.println("ONE through THREE, Dingus.");
         }
         return;
//print error message, “invalid option”
//end executeAdventureChoice
      
      }
   
      public static void generateRandomMonster () {
         System.out.println();
         
    
          int Monster = Rand.nextInt(3);
         
         if (Monster == WOLF)
         {
            createWolf ();
         }
         
         else if (Monster == CULTIST)
         {
            createCultist ();
         }
         else if (Monster == FARMER)
         {
            createFarmer ();
         } 
      }
   
      public static void createDragon () {
         monsterName = "5-Horned Black Metal Dragon";
         monsterAttack = 50;
         monsterHealth = 1000;
         monsterExperience = 20;
      }
      
      public static void runCombatLoop () {
         isFighting = true; 
         
        while (isFighting & isPlayerAlive() && isMonsterAlive()) 
        { 
            runCombatRound ();
            
         }
         
      }
   
      public static void printEndMessage () 
         {
         gameStatus = Status.LOSE;
         System.out.println();
         System.out.println(gameStatus);
         if (gameStatus == Status.LOSE) {
            System.out.println("Covered in blood, dirt, and what might be your own feces, you slink back to your flock.");
         }
    
      
         if (gameStatus == Status.WIN){
            System.out.println("Wow, Who knew that a goat could defeat a dragon? ...Especially since you have no hands to hold your sword....");   
         }
     
      
         if (gameStatus == Status.QUIT){ 
            System.out.println("Yeah, like you've got better things to do.");
         }
    
         }
         
         public static void createWolf ()
         {
            monsterName = "WOLF";
            System.out.println("You hear a Wolf's Growl. He's hungry.");
            monsterHealth = 8 + Rand.nextInt(5);
            monsterAttack = 75 + Rand.nextInt(25);
            monsterExperience = 1; 
         }
         
         public static void createCultist ()
         {
            monsterName = "SATANIC CULTIST";
            System.out.println("A Satanic Cultist is here for your blood!");
            monsterAttack = 12 + Rand.nextInt(5);
            monsterHealth = 100 + Rand.nextInt(25);
            monsterExperience = 3;
         }
         
         public static void createFarmer ()
         
         {
            monsterName = "INAPPROPRIATELY EXCITED FARMER";
            System.out.println("Either that farmer has an ear of corn in his pocket or he's happy to see you!");
            monsterAttack = 15 + Rand.nextInt(5);
            monsterHealth = 150 + Rand.nextInt(50);
            monsterExperience = 5;
         }
         
         public static boolean isPlayerAlive ()
         {
            System.out.println();
            boolean playerlives;
            if (heroHealth >0) 
            {
            return true;
            }
            else
            {
            return false;
            }
         }
         
         public static boolean isMonsterAlive ()
         {
            if (monsterHealth > 0)
            {
               return true;
            }
            else
            {
               return false;
            }
         }
         
         public static void runCombatRound ()
         {
            
            printCombatPrompt();
            int choice = input.nextInt();
            executeCombatChoice(choice);
            if(isFighting = true);
            {
               monsterAttack();
               updateCombatResults();
               printResultMessage();
            }
           }
      
      public static void monsterAttack ()
      {
         if (isMonsterAlive()){
         System.out.println();
         int damage = calculateMeleeDamage(monsterAttack);
         heroHealth-=damage;
         System.out.println("The " +monsterName + " Attacks you with " + damage + " damage");
         System.out.println();
         }
         
      }
      
      
      public static void updateCombatResults ()
      {
         if (!isMonsterAlive())
         {
            levelUp ();
            gameStatus = Status.CONTINUE;
         }
         if (!isPlayerAlive())
         {
            gameStatus = Status.LOSE;
         }
       
      } 
      
       public static void printResultMessage ()
      {
         if (!isMonsterAlive())
         {
         System.out.println("You defeated the " + monsterName + "!");
         }
         if (!isPlayerAlive())
         {
            System.out.println("The " + monsterName + " completely destroyed your body.");
         }
      }
      
      public static boolean executeCombatChoice (int choice)
      {
         
         System.out.println();
         //page 45
         boolean combatTrue = true;
            
         String description = "";
            
         if (choice == MELEE)
         {
         description = meleeOption ();
         System.out.println(description);
         }
         
         else if (choice == MAGIC)
         {
            combatTrue = hasEnoughMana();
            description = magicOption ();
            System.out.println(description);
         }
         
         
         
         else if (choice == CHARGE)
         {
         description = chargeOption();
         System.out.println(description);
         }
         else if (choice == RUN)

         {
         description = fleeOption();
         System.out.println(description);
         }
         else
         {
         combatTrue = false;
         System.out.println("THAT'S NOT A COMMAND OPTION");
         }
         return combatTrue;
         //print value of description variable
        // return valid choice  
      }
      public static String meleeOption ()
      {
         System.out.println();
         int damage = calculateMeleeDamage(heroAttack);
         monsterHealth -= damage;
         String Melee  = "You attack the " + monsterName + " with your sword for " + damage + " damage.";
         return Melee;
      }
      
      public static boolean hasEnoughMana ()
      {
         System.out.println("hasEnoughMana");
        if (heroMagic >= 3)
        {
           return true;
        }
        else
        {
           return false;
        }
      }
      
      public static String magicOption ()
      {
         int damage = calculateMagicDamage(monsterHealth, heroMagic);
         monsterHealth -= heroMagic;
         getMagicText();
         calculateManaLeft();
         String Magic = "You use your magic to boil the" + monsterName + "'s innards.";
         return Magic;
      }
      
      public static String chargeOption ()
      {
         heroMagic ++;
         return "You bleat and your magic power increases.";
      }
      
      public static String fleeOption ()
      {
       //  isFighting = false;
         System.out.println();
         return "You ran away crying!";
      }
      
      public static void levelUp ()
      {
         heroLevel += monsterExperience;
         heroAttack += monsterExperience;
         heroHealth = 50 + Rand.nextInt(31);
         System.out.println("You've leveled up!");
         System.out.println("You're level " + heroLevel);
         if (monsterName == "5-Horned Black Metal Dragon"); {
            gameStatus = Status.WIN;
         }
         gameStatus = Status.CONTINUE;
      } 
      
      public static void printCombatPrompt ()
      {
         System.out.println(monsterName);
         System.out.println("Health: " + monsterHealth);
         System.out.println();
         System.out.println("Your Health: " +heroHealth);
         System.out.println("Your Magic: " +heroMagic);
         System.out.println();
         System.out.println("1. Sword Attack");
         System.out.println("2. Cast Spell");
         System.out.println("3. Charge Mana");
         System.out.println("4. Run Away");
         System.out.println();
      }
      
      public static int calculateMeleeDamage (int attackPower) 
      {
         System.out.println();
         return Rand.nextInt(attackPower);
       
         
      }
      
      public static int calculateMagicDamage (int health, int magic)
      {
         int damage = 0;
         if(heroMagic >= 3)  //you need to use the same variable as the parameter
         {
            damage = health /2;
         }
         return damage;
      }
      public static String getMagicText ()
      {
         String magicText;
         if (heroMagic >=3)
         {
            hasEnoughMana();
            magicText = "You cast Necrosis on the " + monsterName;
         }
         else
         {
            magicText = "Your mana is insufficient.";
         }
         return magicText;
      }
      
      public static void calculateManaLeft ()
      {
        if(heroMagic >= 3)
        {
           hasEnoughMana();
           heroMagic -= 3;
        }
      }

   }
